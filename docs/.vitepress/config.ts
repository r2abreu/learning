import { defineConfig } from "vitepress";

export default defineConfig({
  title: "Notes",
  outDir: "../public",
  themeConfig: {
    search: {
      provider: "local",
    },
    sidebar: {
      "/": [
        {
          text: "Home",
          items: [
            {
              text: "Reference",
              items: [
                { text: "JavaScript", link: "/javascript/index" },
                { text: "Ruby", link: "/ruby/index" },
              ],
            },
          ],
        },
      ],
      "/ruby/": [
        {
          text: "Ruby",
          items: [],
        },
      ],
      "/javascript/": [
        {
          text: "JavaScript",
          items: [
            {
              text: "Kyle Simpson",
              items: [
                { text: "Scopes", link: "/javascript/kyle_simpson/scopes" },
                { text: "Types", link: "/javascript/kyle_simpson/types" },
              ],
            },
          ],
        },
      ],
    },
  },
});
